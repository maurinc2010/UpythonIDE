#! /usr/bin/python
#-*- coding: utf-8 -*-

from PyQt5.Qt import *
from ...UI.init_proyecto import *
from PyQt5.QtWidgets import QDialog


class new_proyecto(QDialog):
    def __init__(self, metodo):
        QtWidgets.QDialog.__init__(self)
        self.ui=Ui_Dialog()
        self.ui.setupUi(self)
        self.setWindowTitle("Crear Nuevo Proyecto")
        self.ui.buscar.clicked.connect(self.ruta)
        self.ui.add_hardware.clicked.connect(self.add_hardware)
        self.fname=""
        self.metodo =metodo
        
    def get_list_harw(self):
        return self.ui.list_harw.currentText()
        
    def add_hardware(self):
        self.metodo.add_hardware()
    
    def view_hardware(self, text):
        self.ui.set_hardware.setText(text)
        
    def get_name_proyect(self):
        return self.ui.name_proyecto.currentText()
        
    def ruta(self):
        dialogo=QDialog()
        dir = QtWidgets.QFileDialog.getExistingDirectory(dialogo,'Set dir', '/home')
        if dir:
            self.view_ruta(dir)
        print('asignando ubicacion')
    
    def view_ruta(self, text):
        self.ui.ruta.setText(text)
        
    def get_ruta(self):
        return self.ui.ruta.text()
