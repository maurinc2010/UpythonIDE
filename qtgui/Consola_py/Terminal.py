#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import sys

from PyQt5.Qt import *#QtCore, QtGui

#from python_highlighter import Highlighter
from .python_shell import PythonShell
#from PyQt5.QtCore import QObject, pyqtSignal

from PyQt5.Qsci import QsciScintilla, QsciLexerPython
from .BaseScintilla import *
HEAD =  "Python " + sys.version + " on " + sys.platform +'\n'
HELP = QApplication.translate("PythonShell", "Commands available:") + ' "clear", "restart"\n'

START = ">>>  "
NEW_LINE = "... "


########################################################################
class Terminal(BaseScintilla):
    
    #----------------------------------------------------------------------
    def __init__(self, widget=None, checkbox=None):
        super(Terminal, self).__init__(widget)
        self.lexer=QsciLexerPython()
        self.setLexer(self.lexer)
        self.linea=0
        #SCI_SETSCROLLWIDTH
        #self.SendScintilla(QsciScintilla.SCI_SETHSCROLLBAR, 0
        self.SendScintilla(QsciScintilla.SCI_SETSCROLLWIDTH, 550)
        #self.auto_text = QsciAPIs(self.lexer)
        #self.auto_text.prepare()
        #self.setAutoCompletionThreshold(1)
    ## Tell the editor we are using a QsciAPI for the autocompletion
        #self.setAutoCompletionSource(QsciScintilla.AcsAll)
        #self.setAutoIndent(True)
        #self.autoCompleteFromDocument ()
        #self.setAutoIndent(True)
        
        
        self.add_line(HEAD)
        self.add_line(HELP)
        
        
        #self.appendPlainText(START)
        
        self.add_line(START)
        self.moveCursorToEndOfDocumentLine()

        self.extra_args = {}
        self.shell = PythonShell()

        self.historial = []
        self.index_historial = 0

        self.multiline_commands = []
        
    def add_line(self, text):
        for i in text:
            if i =="\n":
                self.linea=self.linea+1
        if text== NEW_LINE:
            self.linea=self.linea+1
        self.append(text)
        self.setCursorPosition(self.linea+1, len(START))
        
    def end_line(self):
        self.setCursorPosition(self.linea, len(START))
    #----------------------------------------------------------------------
    def log_output(self, text, level=None):

        if hasattr(self, "checkbox_%s" % level.lower()):
            if not getattr(self, "checkbox_%s" % level.lower()).isChecked(): return

        if level:
            text = text.replace("\n", "\n[%s] "%level)
            text = text.replace("\\n", "\\n[%s] "%level)
            text = "[%s] "%level + text
        if text:
            self.moveCursorToEndOfDocumentLine()
            a =self.shell.run('print("""%s""")'%text.replace('"', "'"))
            self.add_line(a)
            self.moveCursorToEndOfDocumentLine()
            #self.moveCursor(QTextCursor.End)

        self.repaint()


    #----------------------------------------------------------------------
    def write(self, text):
        if not self.checkbox_debug.isChecked(): return
        return self.log_output(text[:-1], level="DEBUG")

    #----------------------------------------------------------------------
    def keyPressEvent(self, event):
        #
        #self.end_line()
        #self.moveCursorToEndOfDocumentLine()
        rr ,  index =self.getCursorPosition()
        if event.key() in (Qt.Key_Enter, Qt.Key_Enter-1):
            self.end_line()
            self.moveCursorToEndOfDocumentLine()
            self.index_historial = 0
            super(Terminal, self).keyPressEvent(event)
            
            
            command = self.get_command()
            if self.multiline_commands:
                self.multiline_commands.append(command)
                #return

                if command.endswith("\n... \n"):
                    #mcommand = ";".join(self.multiline_commands)
                    command = command.replace("\n", ";")
                    command = command.replace(":;", ":").replace(":;", ":").replace(":;", ":")
                    command = command.replace(NEW_LINE, "")
                    command = command[:-2]
                    command += "\n"
                    if self.run_default_command(command):
                        self.add_line(STAR)
                        #self.append(STAR)
                        self.multiline_commands = []
                        return
                    else:
                        self.multiline_commands = []
                        pass

                else:
                    self.add_line(NEW_LINE)
                    #self.append(NEW_LINE)
                    return

            elif command.endswith(":\n"):
                self.multiline_commands.append(command)
                self.add_line(NEW_LINE)
                #self.append(NEW_LINE)
                return

            elif self.run_default_command(command):
                self.add_line(START)
                #self.append(START)
                return
            if len(command) > 1:
                self.historial.append(command.replace("\n", ""))

            if not command.isspace():
                
                log=self.shell.run(command)
                #self.add_line(log)
                self.insert(log)
                for i in log:
                    if i =='\n':
                        rr=rr+1
            #self.add_line(START)
            self.append(START)
            self.linea=rr+1
            self.setCursorPosition(self.linea, len(START))

        elif event.key() == Qt.Key_Backspace:
            if not self.get_command(): return
            else: super(Terminal, self).keyPressEvent(event)

        elif event.key() == Qt.Key_Up:
            if len(self.historial) >= self.index_historial + 1:
                self.index_historial += 1
                self.moveCursorToEndOfDocumentLine()
                self.deleteCurrentLine()
                self.insert(START)
                self.moveCursorToEndOfDocumentLine()
                self.append(self.historial[-self.index_historial])
                self.moveCursorToEndOfDocumentLine()

        elif event.key() == Qt.Key_Down:
            if len(self.historial) >= self.index_historial - 1:
                self.index_historial -= 1
                if self.index_historial <= 0:
                    self.index_historial += 1
                    return
                self.moveCursorToEndOfDocumentLine()
                self.deleteCurrentLine()
                self.insert(START)
                self.moveCursorToEndOfDocumentLine()
                self.append(self.historial[-self.index_historial])
                self.moveCursorToEndOfDocumentLine()

#        elif event.key() == Qt.Key_Left:
#            if self.no_overwrite_start():
#                super(Terminal, self).keyPressEvent(event)

#        elif event.key() == Qt.Key_Tab:
#            tc = self.textCursor()
#            tc.movePosition(tc.WordLeft, tc.KeepAnchor)
#            word = tc.selectedText()
#            object_ = None
#            attr_ = ""
#
#            tc.movePosition(tc.WordLeft, tc.KeepAnchor)
#            n_word = tc.selectedText()
#
#            if n_word == ".":
#                tc.movePosition(tc.WordLeft, tc.KeepAnchor)
#                object_ = tc.selectedText()
#
#            if n_word.startswith("."):
#                tc.movePosition(tc.WordLeft, tc.KeepAnchor)
#                tc.movePosition(tc.WordLeft, tc.KeepAnchor)
#                if tc.selectedText().startswith("."):
#                    while tc.selectedText().startswith("."):
#                        tc.movePosition(tc.WordLeft, tc.KeepAnchor)
#                else:
#                    tc.movePosition(tc.WordRight, tc.KeepAnchor)
#
#                object_, attr_ = tc.selectedText().rsplit(".", 1)
#
#
#            if n_word.endswith("."):
#                tc.movePosition(tc.WordLeft, tc.KeepAnchor)
#                if tc.selectedText().startswith("."):
#                    while tc.selectedText().startswith("."):
#                        tc.movePosition(tc.WordLeft, tc.KeepAnchor)
#                else:
#                    tc.movePosition(tc.WordRight, tc.KeepAnchor)
#
#                object_, attr_ = tc.selectedText().rsplit(".", 1)
#
#            if object_:
#                objects_ = object_.split(".")
#                s = self.shell.statement_module
#                for ob in objects_:
#                    s = getattr(s, ob)
#
#                options = filter(lambda x:x.startswith(attr_), s.__dict__.keys())
#
#            elif word:
#                s = self.shell.statement_module
#                options = filter(lambda x:x.startswith(word), s.__dict__.keys())
#
#            options = filter(lambda x:not x.startswith("__"), options)
#
#            common = os.path.commonprefix(options)
#            if common and word != common: options = [common]
#
#            if len(options) > 1:
#                self.moveCursor(tc.StartOfLine)
#                self.insertPlainText("\t".join(options)+"\n\n")
#                self.moveCursor(tc.End)
#
#            elif len(options) == 1:
#                self.moveCursor(tc.End)
#
#                if object_:
#                    tc.movePosition(tc.WordRight, tc.KeepAnchor)
#                    while "." in tc.selectedText():
#                        tc.movePosition(tc.WordRight, tc.KeepAnchor)
#                    tc.removeSelectedText()
#
#                elif word:
#                    tc.movePosition(tc.WordRight, tc.KeepAnchor)
#                    tc.removeSelectedText()
#
#                self.append(options[0])
#                self.moveCursor(tc.End)


        else:
            super(Terminal, self).keyPressEvent(event)


    #----------------------------------------------------------------------
    def get_command(self):
        plain = self.getText()
        comand = plain[plain.rfind(START):]
        return comand[len(START):]
        


    #----------------------------------------------------------------------
    def wheelEvent(self, event):
        if event.modifiers() == Qt.ControlModifier:
            self.step_font_size(event.delta())

        else: super(Terminal, self).wheelEvent(event)


    ##----------------------------------------------------------------------
    #----------------------------------------------------------------------
    def step_font_size(self, delta):
        font = self.font()
        size = font.pointSize()
        if delta > 0: size = size + 1
        else: size = size - 1

        self.setStyleSheet("""
        QPlainTextEdit {
            background-color: #333;
            color: #FFFFFF;
            font-family: mono;
            font-weight: normal;
            font-size: %dpt;
        }
        """%size)


    #----------------------------------------------------------------------
    def set_extra_args(self, *args, **kwargs):
        for key in kwargs:
            setattr(self.shell.statement_module, key, kwargs[key])
        self.extra_args.update(kwargs)

    #----------------------------------------------------------------------
    def no_overwrite_start(self):
        """No move the cursor over >>>"""
        line, position  = self.position('cursor')
        plain = self.getText()
        index = plain.rfind("\n")
        if position > index:
            return  (position - index) > len(START) + 1
        #if position < index: self.moveCursor(QtGui.QTextCursor.End)


    #----------------------------------------------------------------------
    def run_default_command(self, command):
        try:
            command = command.replace("\n", "")
            run = getattr(self, "command_"+command, None) 
            if run: 
                run()
        except:
            return False

        return bool(run)


    #----------------------------------------------------------------------
    def command_clear(self):
        self.clear()
    #----------------------------------------------------------------------
    def command_restart(self):
        self.shell.restart()
        self.clear()
        self.linea=0
        self.add_line(HEAD)
        self.add_line(HELP)
        self.moveCursorToEndOfDocumentLine()
        self.set_extra_args(**self.extra_args)
        
    def closeEvent(self, event):
    #    """Terminar el thread y establecer el stdout original"""rr ,  index =self.getCursorPosition()
    #    sys.stdout = self.original_stdout
        self.stop()
        #QMainWindow.closeEvent(self, event)
        
class Window(QMainWindow):
    
    def __init__(self):
        # Inicializar la clase padre
        QMainWindow.__init__(self)
        
        # Tamaño y título
        self.resize(500, 300)
        self.setWindowTitle("Consola Recursos Python")
        
        # Editor - resaltador de sintaxis
        
        
        #self.terr = QsciScintilla(self)
        #self.terr.setLexer(QsciLexerPython())
        self.ter=Terminal(self)
        #self.ter.setScrollWidth(200)
        #self.scintilla = SyntaxHighliter(self)
        #self.ter.setGeometry(QRect(10, 10, 4, 250))
        #self.ter(QsciScintilla.SCI_SETHSCROLLBAR, 0)
        
        
        # Ejecutar la consola en un nuevo hilo
        #self.console = GUIConsole(self.scintilla)
        #self.console.start()
        
        # Ajustar el editor a la ventana
        self.setCentralWidget(self.ter)
        
    
    def closeEvent(self, event):
    #    """Terminar el thread y establecer el stdout original"""rr ,  index =self.getCursorPosition()
    #    sys.stdout = self.original_stdout
        self.scintilla.stop()
        QMainWindow.closeEvent(self, event)

        
        
if __name__ == "__main__":
    app = QApplication([])
    window = Window()
    window.show()
    app.exec_()
