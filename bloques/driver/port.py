#! /usr/bin/env python2
#-*- coding: utf-8 -*-
#
# This file is part of pySerial - Cross platform serial port support for Python
# (C) 2010-2015 Chris Liechti <cliechti@gmx.net>
#
# SPDX-License-Identifier:    BSD-3-Clause
"""\
Some tests for the serial module.
Part of pyserial (http://pyserial.sf.net)  (C)2010 cliechti@gmx.net
Intended to be run on different platforms, to ensure portability of
the code.
For all these tests a simple hardware is required.
Loopback HW adapter:
Shortcut these pin pairs:
 TX  <-> RX
 RTS <-> CTS
 DTR <-> DSR
On a 9 pole DSUB these are the pins (2-3) (4-6) (7-8)
"""

#import unittest
from time import time
import serial
import serial.threaded
import serial.tools.list_ports
#from PyQt4 import QtCore, QtGui
from PyQt4.Qt import *
from PyQt4.QtCore import pyqtSignal
from math import*

from tree import *
from variables import *

import re



class comunicacion(QThread):

    """Clase que establece comunicación con el port COM, usa la clase QThread para
    ejecutar un hilo de manera independiente y evitar que la interfaz se bloque por
    la alta tasa de transmicion de datos.

    Los datos resividos por el puerto, son del tipo string, por lo que  se procesan para identificar la naturaleza
    del dato resivido. Los datos recividos conservan la siguiente estructura.

    nombrevar=dato

    nombrevar : hace refrencia al nombre o identificador del dato trasnmitido.
    "=" : Indicador de separación en la cadena, marca donde termina el identificador y empieza el valor asociado.
    dato : Valor asociado a la cadena resivida. Puede ser del tipo float, int, double, str.. etc

    Esta estructura del dato resivido se conserva para asignaciónes simples de un solo valor a una variable
    para el caso de vectores, estos se procesan teniendo encuenta el siguiente algoritmo.

    1. Se transmite el identificador.
        int:        vector de variables enteras.
        float:     vector de variables float.
        double: Vector de variables double.
        char:     Vector de variables char.

    2. Se transmiten los elementos del vector.

    3. Se transmite el simbolo de parada # y fin de transmicion del vector

    señales.

        disco    :    se emite cuando se produce una excepcion del puerto serial
        plot     :    se emite cuando se va a actualizar las graficas"""
    #control de Tm-------------------
        #self.t=0

    #plot=pyqtSignal(object)
    disco=pyqtSignal(object)
    def __init__(self,clas,port=None,parent = None):
        """Establecimiento de las variables globales

            self.dis : lista de puertos disponibles
            self.serial: Configuración del puerto usado"""
        #super(comunicacion, self).__init__()
        QThread.__init__(self, parent)
        self.exiting = False
        self.clas=clas
        #self.plot=plots(clas)
        self.t_t=0.
        self.dis=self.scan_port()
        self.serial=serial.Serial(port,baudrate=4000000, timeout=0.1)
        self.the=None # serial.threaded.ReaderThread

        self.variables=variables(clas)
        self.tree=tree(clas, self.variables.variable)
        self.signal=clas.signal

        self.vector=False
        self.vec=[]
        self.name=""
        self.val=""

    def set_port(self,port):
        """Cambia el puerto usado por la comunicación"""
        self.serial.port=port

    def conect(self):
        """Establece si el puerto deseado esta disponible y se puede establecer
        la comunicación, en caso adverso se emite una señal para gestionar la
        imposibilidad de conectar"""
        try:
            self.serial.open()
        except:
            self.disco.emit("o")

    def close(self):
        """Cierra el puerto usado para la comunicación"""
        self.serial.close()

    def string_(self, a):
        b=""
        for i in a:
            if i=="=":
                b=b+" "+i+" "
            else:
                b=b+i

        a=b+" "
        s=re.match( r'(.*) = (.*?) .*', a, re.M|re.I)
        #print a
        self.name=s.group(1)
        self.val=s.group(2)

    def digital_vol(self, digital):
        return (float(digital)/205.214)-12.418


    def get_datos(self, a, t):
        #name=""
        #val=""
        b=""
        try:
            a=float(a)
            if self.vector:
                self.vec.append(a)
        except:
            self.string_(a)
            #print self.name
            if not re.search('=', self.name):
                #print self.name
                c=""
                #y=False
                for i in self.name:
                    if re.search('\W', i) :
                        c=""
                        #y=True
                    else:
                        c=c+i
                self.name=c
                #print self.name
#                if y:
#                    self.variables.set_val(self.name, self.val)
#                    print self.variables.variable
#                    self.terminate()
                self.variables.set_val(self.name, self.val)
                #print self.variables.variable
            else:
                #print "no separada\n"
                #print self.val
                #print a
                d=False
                for i in a:
                    if i=="=" and not d:
                        d=True
                        self.name=b
                        l=""
                        #print b
                        for m in b:
                            if m!="\r":
                             l=l+m
                            else:
                                l=""
                        self.name=l
                        self.variables.set_val(self.name, self.val)
                        b=""
                    else:
                        b=b+i

                #v=re.sub("\d","",  self.name)
                #print v
                #v=re.sub("=","", v)
                #self.name=re.sub("\s","", v)
                #print v
                #print self.name

        #print self.variables.variable
        self.val=self.digital_vol(self.val)
        self.treee(self.name)
        if self.name in self.signal.graps:
            self.signal.vector(self.name, self.val, t)
                #self.terminate()

            #print self.name
            #print self.val
        #if len(self.variables.variable)>3:
        #    self.terminate()
        #if self.name!="out_1":
         #   self.terminate()

    def treee(self, name):
        if self.tree.search(name):
            #print self.variables.variable
            self.tree.settextchild(name)
            #print self.tree.arbol
        else:
            self.tree.addvar(name)
            #print self.tree.arbol

    def run(self):
        """Ejecución del programa de muetreo en el hilo independiente al principal"""
        #print "casa"
        while self.exiting==False:
            #star_time=time()
            try:
                a=self.serial.readline()
                if a!="":
                    #print "cadena resivida : "+ a +"\n"
                    #end_time=time()-star_time  #Calcula el Tm entre muestras
                    #print end_time
                    #print a
                    #print self.t_t
                    #self.t_t=end_time+self.t_t
                    #print self.t_t
                    star_time=time()
                    self.get_datos(a,star_time)#end_time)#self.t_t)
            except serial.SerialException:
                self.disco.emit("d")
                self.terminate()
                #print "dipositivo desconectado"

#def read_line(self):
#        """Recive una linea desde el puerto"""
#        print self.serial.readline()

    def scan(self):
        """Función para actualizar los puertos disponible y que pertenecen a la
        tarjeta DSP"""
        self.dis=self.scan_port()

    def scan_port(self):
        """Función para buscar los puertos disponible y que pertenecen a la
        tarjeta DSP"""
        m = serial.tools.list_ports.comports()
        p=[]
        if len(m)!=0:
            for i in m:
                if i.pid==10 and i.vid==1240:
                    p.append(i)
                else:
                    p="Conecte Tarjeta"
                    return p
            return p
        else:
            p="Conecte Tarjeta"
            return p

    def write_par(self, p):
        self.serial.write(str(p))




