#! /usr/bin/python
#-*- coding: utf-8 -*-


import sys

#from time import time
#from guidata.qt import*
from PyQt5.Qt import *
#from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QMessageBox
from ..UI.main import *
import os

os.environ.setdefault('QT_API', 'pyqt5')
assert os.environ['QT_API'] in ('pyqt5', 'pyqt', 'pyside')
API = os.environ['QT_API']
print (API)

##barra menu
from ..widget.barra_menu.archivo.Archivo import *
from ..widget.barra_menu.edicion.Edicion import *
#from ..widget.barra_menu.flowchart.flowchart import *
from ..widget.barra_menu.hardware.hardware import *

from ..Consola_py.Terminal import *

from .metodos import *



if sys.platform.startswith('darwin'):
    rsrcPath = ":/images/mac"
else:
    rsrcPath = ":/images/win"


class graphiscdsp(QMainWindow):
    """Gui tipo osiloscopio para tarjeta DSP, envio de comandos de PID y PWM"""
    def __init__(self,splash_write,  *args):
        QMainWindow.__init__(self)
        self.ventana=Ui_MainWindow()  #Clase de la ventana principal
        self.ventana.setupUi(self)
        self.setWindowTitle("GraphicsDsp")
        self.ventana.statusbar.showMessage(("Welcome to graphisc dsp application!"), 5000)

        #****Metodos de la clase de la ventana principal************
        self.metodos_ui=metodos_ui(self)
        self.metodos_ui.init_ventana()

        #****Clases de las diferentes cintas************************
        self.ventana.dockmenu.setTitleBarWidget(QtWidgets.QWidget(None))
        self.archivo=Archivo()
        self.ventana.menu_1.addTab(self.archivo, ("Proyecto"))

        self.edicion=Edicion(self)
        self.ventana.menu_1.addTab(self.edicion, ("Edicion"))

        #self.flowchart=flowchart(self)
        #self.ventana.menu_1.addTab(self.flowchart, ("Flowchart"))

        self.hardware=hardware(self)
        self.ventana.menu_1.addTab(self.hardware, ("Debbug"))
        self.centrar()
        
        # consola
        self.consola=Terminal(self)
        self.metodos_ui.add_tap_consolas("Shell Python", self.consola)
        #self.metodos_ui.ocultar_logo()
        #self.metodos_ui.mostrar_consolas()
        
        
        
        #self.ventana.pestanas.addTab(self.consola.scintilla, (""))
        #self.ventana.pestanas.setTabText(self.ventana.pestanas.indexOf(a),  "EDITOR")
    
 
 

    def editor_cm(self,f):
        self.editor=Editor()
        self.editor.open(f)
        self.editor.setObjectName(("Filtro.pde"))
        self.ventana.central.addTab(self.editor, (""))

    def show_msm(self, title, msm):
        QMessageBox.information(self,title,msm,QMessageBox.Ok)

    def add_editor(self):
        a=Editor()
        self.ventana.pestanas.addTab(a, (""))
        self.ventana.pestanas.setTabText(self.ventana.pestanas.indexOf(a),"EDITOR")

    def esctructura(self):
        self.ventana.estructura.currentIndex()

    def clear_box(self):
        """Borra todo el contenido del listbox usado para mostrar los puertos disponibles"""
        for i in range(0,self.ventana.port.count()+1):
            self.ventana.port.removeItem(i)

 

    def centrar(self):
        """Ubica la ventana en la mitad de la pantalla"""
        screen = QtWidgets.QDesktopWidget().screenGeometry()
        size =  self.geometry()
        self.move((screen.width()-size.width())/2, (screen.height()-size.height())/2)

def main(args):
    app=QApplication(args)
    frame=graphiscdsp()
    frame.show()
    #frame.ventana.terminal.closedd
    sys.exit(app.exec_())


if __name__=='__main__':
    main(sys.argv)
