#!/usr/bin/env python
#-*- coding: utf-8 -*-

from PyQt5.Qt import *
from qtgui.ide.tap import *
#from qtgui.ide.curvewidget import *
#from qtgui.ide.treeview import *




class metodos_ui():
    """Clase diseña para gestionar las funciones de la ventana principal.
        Permite el acceso a las diferentes clases que controlan el comportamiento de la ventana.
        Utilizando diccionarios se controla las clases importada y agregadas a la ventana principal
        con estos se logra que conocer en cada instante las caracteristicas de cada una de las pestañas agregadas"""
    def __init__(self, ui):
        self.ui=ui.ventana
        self.main=ui
        self.tab_central={} #Relaciona el nombre del tap con la clase del widgwet
        self.list_central={} #Relaciona el index del tap con el nombre del tab
        #self.add_treeview("Debbug")



    def add_tap_herraientas(self,Title,widget):
        self.ui.herramientas.addTab(widget,Title)
        
    def add_tap_consolas(self,Title,widget):
        self.ui.consolas.addTab(widget,Title)

    def set_driver(self, driver):
        self.comunicacion.set_driver(driver)   #clase debug

    def add_treeview(self, title):
        self.tree=treeView(self.tab_central)
        self.ui.herramientas.addTab(self.tree, title)

    def add_var_tree(self, name, value):
        self.tree.set_variable(name, value)

    def init_ventana(self):
        """Inicializa la ventana principal"""
        self.mostrar_logo()
        self.ocultar_herramientas()
        self.ocultar_consolas()

    def ocultar_logo(self):
        """Oculta logo de la ventana principal"""
        self.ui.frame_logo.setVisible(False)
        self.ui.central.setVisible(True)
        
    def ocultar_consolas(self):
        """Oculta la ventana lateral donde se ubican los diferentes componentes"""
        self.ui.dockconsolas.setVisible(False)

    def ocultar_herramientas(self):
        """Oculta la ventana lateral donde se ubican los diferentes componentes"""
        self.ui.dockfile.setVisible(False)

    def mostrar_herramientas(self):
        """Hace visible la ventana lateral visible"""
        self.ui.herramientas_.setVisible(True)
        self.ui.herramientas_1.setEnabled(True)
        self.ui.herramientas_.setEnabled(True)
        self.ui.herramientas.setEnabled(True)
        
    def mostrar_consolas(self):
        """Hace visible la ventana lateral visible"""
        self.ui.dockconsolas.setVisible(True)
        self.ui.dockconsolas.setEnabled(True)
        self.ui.dockWidgetContents.setEnabled(True)
        self.ui.consolas.setEnabled(True)

    def mostrar_logo(self):
        """Hace visible el logo principal de la venta principal"""
        self.ui.frame_logo.setVisible(True)
        self.ui.central.setVisible(False)

    def name_ruta(self, f):
        d=""
        for i in f:
            if i=="/":
                d=""
            else:
                d=d+i
        return d

    def add_tab_central(self, name,type,  widget):
        """Agrega una nueva pestaña a la parte central de la venta """
        self.ui.central.addTab(widget, name)
        index=self.ui.central.indexOf(widget)
        tap=pestana(name, type, index, widget)
        self.tab_central[name]=tap   #Relaciona el nombre con la clase del tap
        self.list_central[index]=name  #Relaciona el index del tab central con el nombre de este
        self.ui.central.setCurrentIndex(index)

    def del_componente(self, index):
        """Elimina información de  una pestaña  de los diccionarios """
        del self.tab_central[self.list_central[index]]
        del self.list_central[index]

    def get_index(self):
        """Devuelve el index de la pestaña selecionada"""
        return self.ui.central.currentIndex()

    def get_name(self):
        """Retorna el nombre del objecto relacionado a la pestaña selecionada"""
        return self.list_central[self.get_index()]

    def get_widget(self):
        """Retorna el objecto usado en la pestaña selecionada"""
        return self.tab_central[self.get_name()].get_widget()

    def get_tipo(self):
        """Retorna el dipo de objecto que se muestra en la pestaña"""
        print(self.get_name())
        return self.tab_central[self.get_name()].get_type()




    #def rename_tab(self, name, tab):
    #    self.ui.central.
