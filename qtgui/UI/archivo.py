# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'archivo.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Form(object):
    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(837, 72)
        self.gridLayout = QtWidgets.QGridLayout(Form)
        self.gridLayout.setObjectName("gridLayout")
        self.abrir = QtWidgets.QPushButton(Form)
        self.abrir.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(":/win/images/win/fileopen.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.abrir.setIcon(icon)
        self.abrir.setIconSize(QtCore.QSize(40, 40))
        self.abrir.setObjectName("abrir")
        self.gridLayout.addWidget(self.abrir, 0, 1, 1, 1)
        self.guardar = QtWidgets.QPushButton(Form)
        self.guardar.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap(":/win/images/win/filesave.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.guardar.setIcon(icon1)
        self.guardar.setIconSize(QtCore.QSize(40, 40))
        self.guardar.setObjectName("guardar")
        self.gridLayout.addWidget(self.guardar, 0, 2, 1, 1)
        self.nuevo = QtWidgets.QPushButton(Form)
        font = QtGui.QFont()
        font.setStyleStrategy(QtGui.QFont.PreferAntialias)
        self.nuevo.setFont(font)
        self.nuevo.setContextMenuPolicy(QtCore.Qt.ActionsContextMenu)
        self.nuevo.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap(":/win/images/win/filenew.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.nuevo.setIcon(icon2)
        self.nuevo.setIconSize(QtCore.QSize(40, 40))
        self.nuevo.setCheckable(False)
        self.nuevo.setObjectName("nuevo")
        self.gridLayout.addWidget(self.nuevo, 0, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 0, 3, 1, 1)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))

from .iconos_rc import *
