#! /usr/bin/python
#-*- coding: utf-8 -*-

#from .config.config import *
from ....editor.editor import * 
#from PyQt5.Qt import *
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QDialog
class metodos():
    def __init__(self, ui, ventana):
        
        self.ventana=ventana.ventana
        self.metodos_ui=ventana.metodos_ui
        self.rname=0
        self.list_text={}
    #    self.dialogo=config()
    #    self.driver=""
        self.ui=ui
        
    def nuevo(self):
        self.rname=self.rname+1
        self.metodos_ui.ocultar_logo()
        self.metodos_ui.mostrar_consolas()
        editor=Editor()
        name="editor "+ str(self.rname)
        self.list_text["@"+name+"@"]=name
        editor.setObjectName("@"+name+"@")
        self.metodos_ui.add_tab_central(name,"EDITOR",  editor)
        self.activar_botons()
            
    def abrir(self):
        dialogo=QDialog()
        fname, filter = QtWidgets.QFileDialog.getOpenFileName(dialogo,'Open file', '/home', "python (*.py)")
        if fname:
            editor=Editor()
            name=self.metodos_ui.name_ruta(fname)
            editor.setObjectName((name))
            editor.open(fname)
            self.metodos_ui.ocultar_logo()
            self.list_text[name]=name
            self.metodos_ui.add_tab_central(name, "EDITOR", editor)
            self.activar_botons()
    
    def guardar(self):
        #dialogo=QDialog()
        print("pausar")
    
    def copiar(self):
        print("stop")
        
    def activar_botons(self):
        self.ui.copiar.setEnabled(True)
        self.ui.pegar.setEnabled(True)
        self.ui.cortar.setEnabled(True)
        
        self.ui.fuente.setEnabled(True)
        self.ui.cursiva.setEnabled(True)
        self.ui.negrita.setEnabled(True)
        
        self.ui.sub_ra.setEnabled(True)
        self.ui.font_px.setEnabled(True)
        self.ui.archivos.setEnabled(True)
        
        self.ui.desacer.setEnabled(True)
        self.ui.reacer.setEnabled(True)
        
        self.ui.run.setEnabled(True)
        self.ui.pause.setEnabled(True)
        self.ui.stop.setEnabled(True)
        self.ui.configurara.setEnabled(True)
        self.ui.cargar.setEnabled(True)
