#!/usr/bin/env python
#-*- coding: utf-8 -*-

class pestana():
    """Clase usada para gestionar las  caracterisctictas de cada una de las pestañas 
        agregadas a los diferentes tabview"""
        
    def __init__(self, name, type,  index, widget):
        self.name=name
        self.index=index
        self.widget=widget
        self.type=type
        
    def get_widget(self):
        return self.widget
        
    def get_name(self):
        return self.name
        
    def get_index(self):
        return self.index
        
    def set_name(self, name):
        self.name=name
    
    def get_type(self):
        return self.type
    
    def __del__(self):
        pass
        
    
    
