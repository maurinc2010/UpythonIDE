#! /usr/bin/python
#-*- coding: utf-8 -*-

from PyQt5.Qt import *
from ...UI.new_hardware import *
from PyQt5.QtWidgets import QDialog

class new_hardware(QDialog):
    def __init__(self, metodos):
        QtWidgets.QDialog.__init__(self)
        self.ui=Ui_Dialog()
        self.ui.setupUi(self)
        self.setWindowTitle("Definir nuevos disopositivos")
        self.ui.esc_port.clicked.connect(self.search_port)
        self.ui.add_name.clicked.connect(self.add_name)
        self.fname=""
        self.metodos=metodos
        
    def search_port(self):
        self.metodos.esc_ports()
        
    def add_name(self):
        self.metodos.add_name()
        
    def get_baudios(self):
        return self.ui.baudios.currentText()
        
    def add_list(self, text):
        self.ui.list_name.addItems(text)
    
    def get_port(self):
        return self.ui.list_port.currentText()
        
    def get_list_name(self):
        return self.ui.list_name.currentText()
    
