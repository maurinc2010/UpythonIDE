# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'new_hardware.ui'
#
# Created by: PyQt5 UI code generator 5.7
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(499, 128)
        self.gridLayout_2 = QtWidgets.QGridLayout(Dialog)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.esc_port = QtWidgets.QPushButton(Dialog)
        self.esc_port.setObjectName("esc_port")
        self.gridLayout.addWidget(self.esc_port, 0, 0, 1, 1)
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 1, 0, 1, 1)
        self.gridLayout_3 = QtWidgets.QGridLayout()
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.list_port = QtWidgets.QComboBox(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.list_port.sizePolicy().hasHeightForWidth())
        self.list_port.setSizePolicy(sizePolicy)
        self.list_port.setObjectName("list_port")
        self.gridLayout_3.addWidget(self.list_port, 0, 1, 1, 1)
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setObjectName("label_2")
        self.gridLayout_3.addWidget(self.label_2, 0, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_3, 0, 1, 1, 1)
        self.label = QtWidgets.QLabel(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label.sizePolicy().hasHeightForWidth())
        self.label.setSizePolicy(sizePolicy)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 2, 0, 1, 1)
        self.gridLayout_4 = QtWidgets.QGridLayout()
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.add_name = QtWidgets.QPushButton(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.add_name.sizePolicy().hasHeightForWidth())
        self.add_name.setSizePolicy(sizePolicy)
        self.add_name.setObjectName("add_name")
        self.gridLayout_4.addWidget(self.add_name, 0, 1, 1, 1)
        self.list_name = QtWidgets.QComboBox(Dialog)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.list_name.sizePolicy().hasHeightForWidth())
        self.list_name.setSizePolicy(sizePolicy)
        self.list_name.setObjectName("list_name")
        self.gridLayout_4.addWidget(self.list_name, 0, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_4, 1, 1, 1, 1)
        self.baudios = QtWidgets.QComboBox(Dialog)
        self.baudios.setObjectName("baudios")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.baudios.addItem("")
        self.gridLayout.addWidget(self.baudios, 2, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        self.buttonBox = QtWidgets.QDialogButtonBox(Dialog)
        self.buttonBox.setOrientation(QtCore.Qt.Vertical)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout_2.addWidget(self.buttonBox, 0, 1, 1, 1)

        self.retranslateUi(Dialog)
        self.baudios.setCurrentIndex(13)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        self.esc_port.setText(_translate("Dialog", "Escanear Puertos"))
        self.label_3.setText(_translate("Dialog", "Nombre"))
        self.label_2.setText(_translate("Dialog", "Puerto"))
        self.label.setText(_translate("Dialog", "Baudios"))
        self.add_name.setText(_translate("Dialog", "Agregar Nombre"))
        self.baudios.setItemText(0, _translate("Dialog", "110"))
        self.baudios.setItemText(1, _translate("Dialog", "300"))
        self.baudios.setItemText(2, _translate("Dialog", "600"))
        self.baudios.setItemText(3, _translate("Dialog", "1200"))
        self.baudios.setItemText(4, _translate("Dialog", "2400"))
        self.baudios.setItemText(5, _translate("Dialog", "4800"))
        self.baudios.setItemText(6, _translate("Dialog", "9600"))
        self.baudios.setItemText(7, _translate("Dialog", "14400"))
        self.baudios.setItemText(8, _translate("Dialog", "19200"))
        self.baudios.setItemText(9, _translate("Dialog", "28800"))
        self.baudios.setItemText(10, _translate("Dialog", "38400"))
        self.baudios.setItemText(11, _translate("Dialog", "56000"))
        self.baudios.setItemText(12, _translate("Dialog", "57600"))
        self.baudios.setItemText(13, _translate("Dialog", "115200"))
        self.baudios.setItemText(14, _translate("Dialog", "128000"))
        self.baudios.setItemText(15, _translate("Dialog", "153600"))
        self.baudios.setItemText(16, _translate("Dialog", "230400"))
        self.baudios.setItemText(17, _translate("Dialog", "256000"))
        self.baudios.setItemText(18, _translate("Dialog", "460800"))
        self.baudios.setItemText(19, _translate("Dialog", "921600"))

