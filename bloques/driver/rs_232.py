#! /usr/bin/python
#-*- coding: utf-8 -*-
import serial
import serial.threaded
import serial.tools.list_ports
from PyQt5.QtCore import pyqtSignal

class driver():
    disco=pyqtSignal(object)
    def __init__(self,port=None,sped=4000000, parent = None):
        
        self.serial=serial.Serial(port,baudrate=sped, timeout=0.1)
        self.dis={}
        
    def set_port(self,port):
        """Cambia el puerto usado por la comunicación"""
        self.serial.port=port
        
        
    def conect(self):
        """Establece si el puerto deseado esta disponible y se puede establecer
        la comunicación, en caso adverso se emite una señal para gestionar la
        imposibilidad de conectar"""
        try:
            self.serial.open()
        except:
            print("No se puede abrir puerto")
        #    self.disco.emit("o")
            
    
    def close(self):
        """Cierra el puerto usado para la comunicación"""
        try:
            self.serial.close()
            print("cerrrando puerto")
        except:
            print("No se puede cerrar puerto")
        
        
    def scan_port(self):
        """Función para buscar los puertos disponible y que pertenecen a la
        tarjeta DSP"""
        m = serial.tools.list_ports.comports()
        p=[]
        if len(m)!=0:
            for i in m:
                if  i.vid==1240:# and i.pid==10 :
                    p.append(i)
                else:
                    p=False
                    return p
            return p
        else:
            p=False
            return p
            
    def scan(self):
        """Función para actualizar los puertos disponible y que pertenecen a la
        tarjeta DSP"""
        self.dis={}
        if self.scan_port()==False:
            self.dis["No port"]="Conecte Tarjeta"
        else:
            for i in self.scan_port():
                self.dis[str(i.device)]=i
        #self.dis=self.scan_port()
