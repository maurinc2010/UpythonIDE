#! /usr/bin/python
#-*- coding: utf-8 -*-

from PyQt5.Qt import *
from .....UI.config_hardware import *
from PyQt5.QtWidgets import QDialog
import os
#import sys
class config(QDialog):
    def __init__(self):
        QtWidgets.QDialog.__init__(self)
        self.ui=Ui_Dialog()
        self.ui.setupUi(self)
        
        self.ui.buscar.clicked.connect(self.buscar)
        self.fname=""
        
    def buscar(self):
        self.fname, filter = QtWidgets.QFileDialog.getOpenFileName(self, 'Open file',os.environ["MICROPY_HOME"]+"/bloques/driver", "python (*.py)")
        if self.fname:
            self.ui.ruta.setText(str(self.fname))
        #else:
        #    self.buscar()
    
    def get_driver(self):
        return self.fname
