#! /usr/bin/python
#-*- coding: utf-8 -*-

from PyQt5.Qt import *
from ...UI.new_name import *
from PyQt5.QtWidgets import QDialog

class new_name(QDialog):
    def __init__(self):
        QtWidgets.QDialog.__init__(self)
        self.ui=Ui_Dialog()
        self.ui.setupUi(self)
        self.setWindowTitle("Definir Nuevo Nombre De Dispositivo")
        
    def get_name(self):
        return self.ui.name.text()
    
