#! /usr/bin/python
#-*- coding: utf-8 -*-

from PyQt5.Qt import *
from bloques.plot.plot import *
#from qtgui.widget.plot.plots import *

class curvewidget(plots):
    """Clase para gestionar los curvewidget agregados a las diferentes pestañas"""
    def __init__(self, top):
        plots.__init__(self)
        self.treeview=top.tree
        self.variable=top.variables
        self.name=1
        self.curve={} ##diccionario de widget de qwt agregados a la pestaña
        self.item={}  ##diccionario para relacionar las variables y los items agregados a cada uno dos qwt
        
    def add_curvewidget(self):
        name="Curve "+str(self.name)
        curve=plot()
        curve.register_all_image_tools()
        self.curve[name]=curve
        self.add_plot(curve)
        self.name=self.name+1
    
    def get_name_tree(self):
        return self.treeview.arbol_2[self.treeview.get_index()]
        
    def add_item(self):
        a=get_name_tree()
        self.curve["Curve 1"].add_item(a)
        self.item["Curve 1"]={a:self.variable[a]}
        self.variable.crear_vector(a)
    
   # def set_value(self):
        
    
