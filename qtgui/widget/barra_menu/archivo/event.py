#!/usr/bin/env python
#-*- coding: utf-8 -*-

from PyQt5 import QtCore
from .metodos import *

class event(metodos):
    def __init__(self, ui):
        self.ui=ui
        
        self.ui.nuevo.clicked.connect(self.nuevo)
        self.ui.abrir.clicked.connect(self.abrir)
        self.ui.guardar.clicked.connect(self.guardar)
