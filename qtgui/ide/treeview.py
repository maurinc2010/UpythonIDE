#! /usr/bin/python
#-*- coding: utf-8 -*-
from bloques.treeview.treeview import *

class treeView(treeview):
    
    """Clase para gestionar el arbol de variables de la ventana principal"""
    def __init__(self, variable):
        treeview.__init__(self)
        self.tab_central=variable
        self.submenu={}
        self.menu.connect(self.create_menu)
        
    def create_menu(self, position):
        """Se genera el menu para determinar donde graficar la variable"""        
        indexes = self.selectedIndexes()
        #index = self.selectedIndexes()[0]
        #print(index.row())
        #crawler = index.model().itemFromIndex(index)
        #p= self.selectedIndexes()
        #print(crawler)#.parent().row())#index.model().itemFromIndex(index))
        try:
            if len(indexes) >=0:
                level = 0
                index = indexes[0]
                while index.parent().isValid():
                    index = index.parent()
                    level += 1
            menu = QMenu()
            self.sub_menu()
            if level == 0:
                for i, n in self.submenu.items():
                    menu.addMenu(n)
#            sub_menu_5=QMenu()
#            sub_menu_5.setTitle("halo")
#            sub_menu_5.addAction(QAction(self.tr("marlon"), self, triggered=self.add_plot))
#            menu.addMenu(sub_menu_5)
            menu.exec_(self.viewport().mapToGlobal(position))
            
        except:
            menu = QMenu()
            menu.addAction("No variable")
        #menu.triggered[QAction].connect(self.menus)
        #self.viewmenu(menu, position)
        #pass
    def get_index(self):
        return self.selectedIndexes()[0].row()
        

    def sub_menu(self):
        for i,n in self.tab_central.items():
            if n.type=='PLOT':
                sub_menu_1=QMenu()
                sub_menu_1.setTitle(str(i))
                for d in n.widget.curve:
                    sub_menu_1.addAction(QAction(self.tr(d), self, triggered=n.widget.add_item))
            self.submenu[i]=sub_menu_1
