#!/usr/bin/env python
#-*- coding: utf-8 -*-

from PyQt5 import QtCore
from .metodos import *
class event(metodos):
    def __init__(self, ui, ventana):  #ui es la citna ,venta es el mainwindows
        metodos.__init__(self, ui, ventana)
        self.ui=ui.ui
        self.ui.config.clicked.connect(self.config)
        self.ui.iniciar.clicked.connect(self.iniciar)
        self.ui.stop.clicked.connect(self.stop)
        self.ui.pausar.clicked.connect(self.pausar)
        self.ui.new_plot.clicked.connect(self.new_plot)
        self.ui.add_plot.clicked.connect(self.add_plot)
        
    
