 #!/usr/bin/env python3
#-*- coding: utf-8 -*-

NAME = "MICROPY IDE"
VERSION = "1.0"
SUBVERSION = "beta.1"

#DESCRIPTION = ""
#LONG_DESCRIPTION = ""

################################################################################

"""-------------------------------------------------------------------------

-------------------------------------------------------------------------"""

################################################################################

import sys
import os

if sys.version_info >= (3, ):
    #Python3
    os.environ["MICROPY"] = "3"
else:
    #Python2
    os.environ["MICROPY"] = "2"


# Python3 compatibility
if os.getenv("MICROPY") is "3":
    import imp
    imp.reload(sys)
else:
    import imp
    imp.reload(sys)
    sys.setdefaultencoding("utf-8")


os.environ["MICROPY"] = NAME
os.environ["MICROPY"] = VERSION
os.environ["MICROPY_SUBVERSION"] = SUBVERSION
os.environ["MICROPY_HOME"] = os.path.abspath(sys.path[0])

# For PyInstaller compatibility
if os.path.exists(os.path.abspath("MICROPY_DATA")):
    os.environ["MICROPY_DATA"] = os.path.abspath("MICROPY_DATA")
else:
    os.environ["MICROPY_DATA"] = os.getenv("MICROPY_HOME")


class bcolors:
    Black = "\033[0;30m"
    Red = "\033[0;31m"
    Green = "\033[0;32m"
    Yellow = "\033[0;33m"
    Blue = "\033[0;34m"
    Purple = "\033[0;35m"
    Cyan = "\033[0;36m"
    LightGray = "\033[0;37m"
    ENDC = "\033[0m"



use_gui=True

# Python3 compatibility
if os.getenv("MICROPY_PYTHON") is "3":
    #Python3
    python_path_modules = os.path.join(os.getenv("MICROPY_DATA"), "python3_requirements")
else:
    #Python2
    python_path_modules = os.path.join(os.getenv("MICROPY_DATA"), "python_requirements")


DEFAULT_STYLE = """
QProgressBar{
    border: 1px solid grey;
    border-radius: 5px;
    text-align: center
}

QProgressBar::chunk {
    background-color: lightblue;
    width: 5px;
    margin: 1px;
}
"""

if os.path.isdir(python_path_modules): sys.path.append(python_path_modules)

sys.path.append(os.path.join(os.getenv("MICROPY_DATA"), "qtgui", "resources"))

if __name__ == "__main__":

    if use_gui:

        #import debugger
        #debugger.Debugger(sys, clear=True)
        #from guidata.qt import*
        from PyQt5 import QtCore
        from PyQt5.QtWidgets import QApplication, QSplashScreen,  QProgressBar
        from PyQt5.QtGui import QPixmap
        #app = QApplication(sys.argv)
        from qtgui.ide.graphiscdsp import graphiscdsp

        app = QApplication(sys.argv)


        #Splash
        splash_pix = QPixmap('splash.jpg').scaled(720, 405, QtCore.Qt.KeepAspectRatio)
        #pixmap = QPixmap("logo.png")
        splash = QSplashScreen(splash_pix, QtCore.Qt.WindowStaysOnTopHint)
        splash.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint | QtCore.Qt.FramelessWindowHint)
        #splash.setEnabled(False)
        
        progressBar = QProgressBar(splash)
        progressBar.setStyleSheet(DEFAULT_STYLE)
        progressBar.setMaximum(10)
        progressBar.setGeometry(10, splash_pix.height()-18, splash_pix.width()-30, 15)
        
        splash.show()
        splash.setStyleSheet("""
            font-family: inherit;
            font-weight: normal;
            font-size: 12pt;
            """)


        def splash_write(msg):
            if not splash is None:
                splash.showMessage("\t\n\n"+msg+"\n", color=QtCore.Qt.black, alignment=QtCore.Qt.AlignBottom)

        splash_write(NAME+" "+VERSION)
        app.processEvents()

       # app.installTranslator(qtTranslator)
        #if trasnlations: app.installTranslator(translator)
        import time
        for i in range(1, 11):
            progressBar.setValue(i)
            t = time.time()
            while time.time() < t + 0.1:
                app.processEvents()
        
       
        frame = graphiscdsp(splash_write=splash_write)
        frame.show()

        if not splash is None:
            splash.finish(frame)

        #For PyInstaller compatibility
        if app is None:
            from PyQt5.QtGui import QApplication
            QApplication.instance().exec_()
        else:
            #sys.exit(app.exec_())
            app.exec_()
