#! /usr/bin/python
#-*- coding: utf-8 -*-

from PyQt5.QtCore import *
from PyQt5.QtGui import *
from PyQt5.Qt import *
from PyQt5 import QtWidgets
from PyQt5.QtCore import pyqtSignal

class treeview(QtWidgets.QTreeView):
    menu=pyqtSignal(object)
    def __init__(self):
        QTreeView.__init__(self)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.arbol={}  ##diccionario para relacionar nombre de las varibles y las posion de los item
        self.arbol_2={}
        self.model=QStandardItemModel()
        self.setModel(self.model)
        self.model.setHorizontalHeaderLabels([self.tr("Variables")])
        self.customContextMenuRequested.connect(self.openMenu)
        
    def set_variable(self, name, value):
        if self.search(name):
            self.settextchild(name, value)
        else:
            self.addvar(name, value)    
        
    def settextchild(self, name, value):
        """Cambia el texto o el valor de los hijos de las varibles """
        self.model.item(self.arbol[name][1], self.arbol[name][2]).child(self.arbol[name][3], self.arbol[name][4]).setText(str(value))
        
    def remove(self, name):
        """Elimina una de las relaciones de las variables y las pociones de los item en el arbol"""
        del self.arbol[name]
        
    def search(self, name):
        """Busca la existencia de una variable dentro del diccionario que contiene las variables agregadas al arbol"""
        return str(name) in self.arbol
        
    def remove_child(self, name):
        """Elimina un hijo del arbol"""
        self.model.itemFromIndex(self.arbol[name][0]).removeRow(0) 
        
    def remove_parent(self, name):
        """Elimina un padre del arbol"""
        self.model.removeRow(self.arbol[name][1])
        
        
    def settextparent(self, name, name_1):
        """Cambia el texto de padre del arbol"""
        v=self.arbol.pop(name)
        self.arbol[name_1]=v
        self.model.item(self.arbol[name_1][1], self.arbol[name_1][2]).setText(str(name_1))
        
    def addItems(self, parent, elements):
        """Agrega todas la varibles al debug"""
        for i in elements:  #i es el name de las variables
            item = QStandardItem(i)  #item es el padre del arbol
            parent.appendRow(item)
            #print parent.QModelIndex()
            #item2 = QStandardItem(i[1][0])
            #item.appendRow(item2)
            item3 = QStandardItem(str(elements[i]))  #ellements[i] es el valor de la variables
            item.appendRow(item3)                            #item3 es el child que contiene el valor de la variable
            
            f=parent.indexFromItem(item)
            p=parent.indexFromItem(item3)
            self.arbol[i]=[f, f.row(), f.column(), p.row(), p.column()]
            self.arbol_2[(f.row())]=i
            #i[0][1]=f.row()
            #i[0][2]=f.column()
            
    def addvar(self, name, value):
        """Agrega una varible al al arbol de  debug"""
        item = QStandardItem(str(name))
        self.model.appendRow(item)
        #    item2 = QStandardItem(tree[1][0])
        #    item.appendRow(item2)
        item3 = QStandardItem(str(value))
        item.appendRow(item3)
                
        f=self.model.indexFromItem(item)
        p=self.model.indexFromItem(item3)
        self.arbol[name]=[f, f.row(), f.column(), p.row(), p.column()]
        self.arbol_2[(f.row())]=name
        
    def openMenu(self, position):
        """Se genera el menu para determinar donde graficar la variable"""
        self.menu.emit(position)
        #print(self.arbol_2)
        
    def viewmenu(self, menu, position):
        menu.exec_(self.viewport().mapToGlobal(position))
        
    def remove_plot(self, q):
        indexes = self.selectedIndexes()
        f=self.model.itemFromIndex(indexes[0]).text()
        self.sig.remove(f)
        #print f
        
        
#    def plot1(self, q):
#        """Se envia la variable al plot 1"""
#        indexes = self.selectedIndexes()
#        print ("plot de signal")
#        f=self.model.itemFromIndex(indexes[0]).text()
#        self.sig.addsignal(f, "plot1")
#        #print f
#        
#        
#    def plot2(self, q):
#        """Se envia la variable al plot 2"""
#        indexes = self.selectedIndexes()
#        f=self.model.itemFromIndex(indexes[0]).text()
#        self.sig.addsignal(f, "plot2")
        #print f
        
#    def fft_1(self, q):
#        """Se envia la fft al plot 1"""
#        indexes = self.selectedIndexes()
#        f=self.model.itemFromIndex(indexes[0]).text()
#        self.sig.addfft(f, "plot1")
#        #print f
#        
#    def fft_2(self, q):
#        """Se envia la fft al plot 1"""
#        indexes = self.selectedIndexes()
#        f=self.model.itemFromIndex(indexes[0]).text()
#        self.sig.addfft(f, "plot2")
#        #print f
