#! /usr/bin/python
#-*- coding: utf-8 -*-

from .config.config import *
import re

class metodos():
    
    """Clase diseña para gestionar las diferentes tareas de la cinta de hardware.driver"""   

    def __init__(self, ui, ventana):
#        self.plots=Plots()
        #self.ventana.central.addTab(self.plots, ("Plots"))
        self.dialogo=config()
        self.ndriver=""
        self.ui=ui
        self.metodos_ui=ventana.metodos_ui
        self.rname=0
        self.state=False
        self.driver=""
        self.dispoci={}
        self.debug=""
        self.pause=False
        self.chang=False
        
    def set_debug(self, debug):
        """Se establece interaccion con la clase debug """
        self.debug=debug
        
    def add_dispositivo(self, disp):
        """Agrega un nuevo dispositivo a la lista de disponibles"""
        for i in disp:
            if  self.dispoci.get(i)==None:
                self.dispoci[i]=i
                self.ui.disp.addItem(str(i))
        self.refres_disp(disp)
                
    def refres_disp(self, disp):
        """Verifica que los dispositivos desconectados se elimen de la lista"""
        d=False
        for i in self.dispoci:
            try:
                disp[i]
            except:
                del self.dispoci[i]
                d=True
                break
        if d:
            self.ui.disp.clear()
            for i in self.dispoci:
                self.ui.disp.addItem(str(i))
            self.refres_disp(disp)        
    
    def config(self):
        """Lanza la ventana de selecion del driver """
        self.dialogo.exec_()
        self.ndriver=self.dialogo.get_driver()        
        self.state=False
        self.chang=False
        if self.ndriver!="":
            self.activar_botons()
            self.crear(str(self.ndriver))
            self.state=True
            self.chang=True
            
    def crear(self, driver):
        """Funcion que toma el nombre del archivo del codigo del driver
            y genera el codigo fuente para poder usarlo """
        self.plantilla=open(os.environ["MICROPY_HOME"]+"/qtgui/widget/barra_menu/hardware/driver.txt", 'r+')
        self.archi=open(os.environ["MICROPY_HOME"]+"/qtgui/widget/barra_menu/hardware/driver.py",'w')
        linea=self.plantilla.readline()
        while linea!="":
            if linea=="#ruta\n":
                d=0
                #m="'."
                m=""
                r=""
                z=""
                f=True
                for i in driver:
                    if d==3:
                        z=""
                        d=0
                    if i=="/" :
                        if f:
                            m=m+r
                            f=False
                        else:
                            m=m+'/'+r
                        r=""
                    elif z==".py":
                        break
                    else:
                        r=r+i
                        z=z+i
                        d=d+1
                l='sys.path.append(' +m+ "')\n"
                #self.archi.write(l)
                k=re.compile(".py")
                r=k.sub("",r )
                tt=re.compile("/")
                m=tt.sub(".", m)
                #tt=re.compile("graphics_dsp.bloques.driver")
                m="graphics_dsp.bloques.driver"
                l="from "+ m +"."+ r + " import *\n"
                self.archi.write(l)
            else:
                self.archi.write(linea)
            linea=self.plantilla.readline()
        self.plantilla.close()
        self.archi.close()
        self.set_driver()
        
    def set_driver(self):  
        """Funcion para importar el archivo que incluye el codigo fuente del driver"""
        from qtgui.widget.barra_menu.hardware.driver import comunicacion
        self.driver=comunicacion()
        #return comunicacion()
        
    def get_class(self):
        """Retorna la clase creada con el codigo fuente del driver, permite acceder a los
            metodos del driver"""
        return self.driver
        
    def get_driver(self):
        """Retorna el nombre del driver selecionado y usado"""
        return str(self.Ndriver)
        
    def get_chang(self):
        """Retorna el estados de la variable que determina si la selecion del driver se ha cambiado"""
        return self.chang
            
    def get_state(self):
        """Retorna el estado de la variable que determina si hay un archivo de driver selecionado"""
        return self.state
            
    def iniciar(self):
        """Metodo para iniciar la adquicion de datos usando el driver selecionado"""
        if self.pause:
            self.driver.driver.conect()
            self.debug.start()
            self.pause=False
            self.ui.iniciar.setEnabled(False)
            self.ui.stop.setEnabled(True)
            self.ui.pausar.setEnabled(True)
        else:
            self.driver.driver.set_port(self.ui.disp.itemText(self.ui.disp.currentIndex()))
            self.driver.driver.conect()
            self.debug.start()
            self.ui.iniciar.setEnabled(False)
            self.ui.stop.setEnabled(True)
            self.ui.pausar.setEnabled(True)
        print("iniciar ")
    
    def pausar(self):
        """Metodo para pausar la adquicion de datos"""
        self.debug.terminate()
        self.driver.driver.close()
        self.pause=True
        self.ui.iniciar.setEnabled(True)
        self.ui.stop.setEnabled(False)
        self.ui.pausar.setEnabled(False)
        print("pausar")
    
    def stop(self):
        """Metodo para detener la adquicion de datos"""
        self.debug.terminate()
        self.driver.driver.close()
        self.pause=False
        self.ui.iniciar.setEnabled(True)
        self.ui.stop.setEnabled(False)
        self.ui.pausar.setEnabled(False)
        print("stop")
        
    def new_plot(self):
        """Agrega una nueva pestaña para plotiar
            falta activar el menu para el titulo, label de los eje y las unidades"""
        self.rname=self.rname+1
        self.metodos_ui.new_plot("plot "+str(self.rname))
        self.ui.add_plot.setEnabled(True)
        
    def add_plot(self):
        """Agrega una nueva interfaz de graficado"""
        self.metodos_ui.add_plot()
    
        
    def activar_botons(self):
        """Activa los botones de la cinta despues que se carga el driver"""
        self.ui.pausar.setEnabled(False)
        self.ui.stop.setEnabled(False)
        self.ui.iniciar.setEnabled(True)
        self.ui.new_plot.setEnabled(True)
        
    
