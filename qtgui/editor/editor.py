#! /usr/bin/python
#-*- coding: utf-8 -*-


import sys
from PyQt5.QtCore import *
from PyQt5 import QtCore, QtGui,Qsci
from PyQt5.QtGui import *
from PyQt5.Qsci import QsciScintilla, QsciScintillaBase, QsciLexerPython
from PyQt5.Qsci import*


class Editor(QsciScintilla, QObject):
    ARROW_MARKER_NUM = 8
    def __init__(self, parent=None):
        super(Editor, self).__init__(parent)
        self.font_text=QFont()
        
        self.extenciones={'py':QsciLexerPython(), 'c':QsciLexerCPP(), 'h':QsciLexerCPP()}
        #configuracion por defecto  tipo de letras texto
        self.font_text.setFamily('Consolas')
        self.font_text.setFixedPitch(True)
        self.font_text.setPointSize(10)
        
        #configuracion por defecto tipo de texto numeracion
        self.font_margin=QFont()
        self.font_margin.setFamily('Consolas')
        self.font_margin.setFixedPitch(True)
        self.font_margin.setPointSize(10)
        
        #selecion de extecion
        self.lexer=self.extenciones['py']
        self.lexer.setDefaultFont(self.font_text)
        self.setLexer(self.lexer)
        
        #colores de fondo de numeración y contracion
        self.set_margen()  #margen derecha
        self.set_numeracion()
        self.set_color_bnumeracion()
        self.set_color_numeracion()
        self.set_color_contracion()
        self.set_contracion()
        
        #Colores y activacion de linea selicionada
        self.enable_set_line()
        self.set_color_line()
        
        # Clickable margin 1 for showing markers
        self.define_mark()
        self.setMarginSensitivity(1, True)
        #self.connect(self,SIGNAL('marginClicked(int, int, Qt::KeyboardModifiers)'),self.on_margin_clicked)
        
        # Don't want to see the horizontal scrollbar at all
        # Use raw message to Scintilla here (all messages are documented
        # here: http://www.scintilla.org/ScintillaDoc.html)
        self.SendScintilla(QsciScintilla.SCI_SETHSCROLLBAR, 0)
        # Current line visible with special background color
        
        #autocompletado
        self.auto_text = Qsci.QsciAPIs(self.lexer)
        self.auto_text.prepare()
        self.setAutoCompletionThreshold(1)
    ## Tell the editor we are using a QsciAPI for the autocompletion
        self.setAutoCompletionSource(QsciScintilla.AcsAll)
        self.setAutoIndent(True)
    
        #self.auto_text.prepare()
        #self.setAutoCompletionSource()
        
        
        #self.setText(open("graphiscdsp.py").read())
        self.autoCompleteFromDocument ()
        #self.autoCompletionSource()
    def open(self, f):
        self.setText(open(str(f)).read())
        
    def add_auto(self, a):
         #self.auto_text = Qsci.QsciAPIs(self.lexer)
         self.auto_text.add(str(a))
         #self.auto_text.prepare()
         
    def enable_set_line(self, a=True):    
        self.setCaretLineVisible(a)
    
    def set_color_line(self, a="#ffe4e4"):  
        self.setCaretLineBackgroundColor(QColor(a))
        
    def define_mark(self):
        self.markerDefine(QsciScintilla.RightArrow,self.ARROW_MARKER_NUM)
        self.setMarkerBackgroundColor(QColor("#ee1111"),self.ARROW_MARKER_NUM)
        
    def on_margin_clicked(self, nmargin, nline, modifiers):
        # Toggle marker for the line the margin was clicked on
        if self.markersAtLine(nline) != 0:
            self.markerDelete(nline, self.ARROW_MARKER_NUM)
        else:
            self.markerAdd(nline, self.ARROW_MARKER_NUM)
        
    def cam_extencion(self, a):
        self.lexer=extenciones[str(a)]
        self.lexer.setDefaultFont(self.font_text)
        self.setLexer(self.lexer)
        
    def cam_letra_texto(self, a):
        self.font_text.setFamily(str(a))
        self.setFont(self.font_text)
        self.lexer.setDefaultFont(self.font_text)
        
    def cam_tam_letra_texto(self, a=10):
        self.font_text.setPointSize(a)
        self.setFont(self.font_text)
        self.lexer.setDefaultFont(self.font_text)
        
    def cam_letra_texto(self, a):
        self.font_margin.setFamily(str(a))
        self.setMarginsFont(self.font_margin)
        
    def cam_tam_letra_texto(self, a=10):
        self.font_margin.setPointSize(a)
        self.setMarginsFont(self.font_margin)
        
    def set_numeracion(self):
        fontmetrics = QFontMetrics(self.font_margin)
        self.setMarginsFont(self.font_margin)
        self.setMarginWidth(0, fontmetrics.width("00000") + 6)
        self.setMarginsBackgroundColor(QColor("#cccccc"))
        self.set_acti_numeracio(True)
        
    def set_acti_numeracio(self, a):
        self.setMarginLineNumbers(0, a)
    
    def set_color_bnumeracion(self, a="#333333"):
        self.setMarginsBackgroundColor(QtGui.QColor(a))
        
    def set_color_numeracion(self, a="#CCCCCC"):
        self.setMarginsForegroundColor(QtGui.QColor(a))
        
    def set_margen(self, a=100):
        self.setEdgeMode(QsciScintilla.EdgeLine)
        self.setEdgeColumn(a)
        self.set_color_margin()
        
    def set_color_margin(self, a="#FF0000"):
        self.setEdgeColor(QtGui.QColor(a))
    
    def set_contracion(self):
        self.setFolding(QsciScintilla.CircledTreeFoldStyle)
         ## Braces matching
        self.setBraceMatching(QsciScintilla.SloppyBraceMatch)
        
    def set_color_contracion(self, a="#99CC66",b="#333300"):
        self.setFoldMarginColors(QtGui.QColor(a),QtGui.QColor(b))
    
if __name__ == "__main__":
    app = QApplication(sys.argv)
    editor1 = Editor()
    editor1.show()
    editor1.setText(open("editor.py").read())
    editor1.autoCompleteFromDocument ()
    sys.exit(app.exec_())
